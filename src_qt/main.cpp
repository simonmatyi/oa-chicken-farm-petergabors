#include <iostream>

#include "chicken.h"
#include <QTextStream>

const QString menuHeader = \
"Please choose from the following options:\n"\
"[1] New chicken\n"\
"[2] Show chickens\n"\
"[3] Stop all chickens\n"\
"[4] Quit\n"
"Your choice: ";

void stopChickens(QTextStream & out, std::list<Chicken> & farm)
{
    out << "Stopping active chickens..." << endl;

    for(Chicken & chicken : farm)
    {
        if(!chicken.isFinished())
        {
            chicken.requestInterruption();
        }
    }
}

int main()
{
    QTextStream out(stdout);
    QTextStream in(stdin);

    {
        bool farmRunning = true;
        std::list<Chicken> farm;

        out << "===== Welcome to Chicken Farm Qt =====" << endl;

        while(farmRunning)
        {
            out << endl << menuHeader;
            out.flush();

            char userInput;

            in >> userInput;
            out << endl;

            switch(userInput)
            {
            case '1':
                {
                    farm.resize(farm.size() + 1);
                    out << "New chicken #" << (farm.size() - 1) << " " << farm.back().name() << endl;
                }
                break;

            case '2':
                if(farm.size() > 0)
                {
                    out << QString("#%1%2%3%4%5")
                           .arg("no", -3)
                           .arg("name", -16)
                           .arg("eggs", -5)
                           .arg("alive", -6)
                           .arg("lastEgg", -8)
                            << endl;

                    int i = 0;
                    for(Chicken & chicken : farm)
                    {
                        out << QString("#%1%2%3%4%5")
                               .arg(i, -3)
                               .arg(chicken.name(), -16)
                               .arg(chicken.eggsLayed(), -5)
                               .arg(chicken.isFinished() ? "no" : "yes", -6)
                               .arg(chicken.isInterruptionRequested() ? "yes" : "no", -8)
                                << endl;
                        i++;
                    }
                }
                else
                {
                    out << "There are no chickens on your farm :(" << endl;
                }
                break;

            case '3':
                stopChickens(out, farm);
                break;

            case '4':
                stopChickens(out, farm);
                farmRunning = false;
                break;

            default:
                out << "Invalid input!" << endl;
                break;
            }
        }
    }

    out << "Bye!" << endl;

    return 0;
}

