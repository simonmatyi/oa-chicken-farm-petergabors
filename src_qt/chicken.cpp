#include "chicken.h"

#include <QDebug>
#include <QRandomGenerator>
#include <QMutexLocker>

Chicken::Chicken(int layingInterval) :
    QThread()
{
    init(layingInterval);
    start();
    moveToThread(this);
}

Chicken::~Chicken()
{
    requestInterruption();
    wait();
}

void Chicken::init(int layingIntervalMsec)
{
    _name = randomName();
    _eggsLayed = 0;
    _layingIntervalMsec = layingIntervalMsec;

    connect(this, &Chicken::newEgg, this, &Chicken::layEgg);
}

const QString & Chicken::name() const
{
    return _name;
}

const QString Chicken::randomName()
{
    return possibleNames[QRandomGenerator::system()->bounded(possibleNames.size() - 1)];
}

int Chicken::eggsLayed()
{
    QMutexLocker guard(&eggsLayed_mutex);
    return _eggsLayed;
}

int Chicken::layingIntervalMsec() const
{
    return _layingIntervalMsec;
}

int Chicken::randomLayingIntervalMsec()
{
    return QRandomGenerator::system()->bounded(minRandomLayingInterval_ms, maxRandomLayingInterval_ms);
}

void Chicken::layEgg()
{
    QMutexLocker guard(&eggsLayed_mutex);
    _eggsLayed++;
}

void Chicken::run()
{
    do
    {
        msleep(static_cast<unsigned long>(_layingIntervalMsec));

        emit newEgg();
    } while (!isInterruptionRequested());
}

const QList<QString> Chicken::possibleNames =
{
    "Anastasia",
    "Annie",
    "Arabella",
    "Beatrice",
    "Bessie",
    "Betsy",
    "Edna",
    "Charlotte",
    "Clarabelle",
    "Daisy",
    "Daphne",
    "Dorothy",
    "Dottie",
    "Estelle",
    "Eloise",
    "Felicity",
    "Florence",
    "Francis",
    "Genevieve",
    "Georgia",
    "Geraldine",
    "Gladys",
    "Gloria",
    "Harriet",
    "Holly",
    "Iris",
    "June",
    "Loretta",
    "Mabel",
    "Maude",
    "Minnie",
    "Matilda",
    "Myrtle",
    "Opal",
    "Pearl",
    "Penelope",
    "Polly",
    "Stella",
    "Sadie",
    "Tillie"
};
