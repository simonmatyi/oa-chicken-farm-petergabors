#pragma once

#include <QThread>
#include <QList>
#include <QString>
#include <QMutex>

class Chicken : public QThread
{
    Q_OBJECT

signals:
    void newEgg();

public:
    Chicken(int layingInterval = randomLayingIntervalMsec());
    ~Chicken();

    void init(int layingIntervalMsec);

    const QString & name() const;
    static const QString randomName();

    int eggsLayed();

    int layingIntervalMsec() const;
    static int randomLayingIntervalMsec();

private slots:
    void layEgg();

private:
    void run();

    QString _name;

    QMutex eggsLayed_mutex;
    int _eggsLayed;

    int _layingIntervalMsec;

    static const QList<QString> possibleNames;
    static const int maxRandomLayingInterval_ms = 10000;
    static const int minRandomLayingInterval_ms = 2000;
};
