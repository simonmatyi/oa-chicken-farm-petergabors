#include <iostream>

#include "chicken.h"
#include <list>
#include <iomanip>

const std::string menuHeader = \
"Please choose from the following options:\n"\
"[1] New chicken\n"\
"[2] Show chickens\n"\
"[3] Stop all chickens\n"\
"[4] Quit\n"
"Your choice: ";

void stopChickens(std::list<Chicken> & farm)
{
    std::cout << "Stopping active chickens..." << std::endl;

    for(Chicken & chicken : farm)
    {
        if(!chicken.dead())
        {
            chicken.slay();
        }
    }
}

int main()
{
    {
        bool farmRunning = true;
        std::list<Chicken> farm;

        std::cout << "===== Welcome to Chicken Farm STL =====" << std::endl;

        while(farmRunning)
        {
            std::cout << std::endl << menuHeader;

            char userInput;

            std::cin >> userInput;
            std::cout << std::endl;

            switch(userInput)
            {
            case '1':
                {
                    farm.resize(farm.size() + 1);
                    std::cout << "New chicken #" << (farm.size() - 1) << " " << farm.back().name() << std::endl;
                }
                break;

            case '2':
                if(farm.size() > 0)
                {
                    std::cout << "#no name            eggs alive lastEgg" << std::endl;

                    int i = 0;
                    for(Chicken & chicken : farm)
                    {
                        std::cout << std::left
                                << "#" << std::setw(2) << i << std::setw(0) << " "
                                << std::setw(16) << chicken.name()
                                << std::setw(5) << chicken.eggsLayed()
                                << std::setw(6) << (chicken.dead() ? "no" : "yes")
                                << std::setw(7) << (chicken.awaitingButcher() ? "yes" : "no")
                                << std::setw(0) << std::endl;
                        i++;
                    }
                }
                else
                {
                    std::cout << "There are no chickens on your farm :(" << std::endl;
                }
                break;

            case '3':
                stopChickens(farm);
                break;

            case '4':
                stopChickens(farm);
                farmRunning = false;
                break;

            default:
                std::cout << "Invalid input!" << std::endl;
                break;
            }
        }
    }

    std::cout << "Bye! " << std::endl;

    return 0;
}

