#include "chicken.h"

#include <random>
#include <iostream>

using std::chrono_literals::operator""ms;

Chicken::Chicken(const std::chrono::milliseconds & layingInterval) :
    std::thread(&Chicken::live, this, std::cref(layingInterval))
{
    initFromConstructor();
}

Chicken::~Chicken()
{
    slay();
    if(joinable())
    {
        join();
    }
}

void Chicken::initFromConstructor()
{
    {
        std::lock_guard<std::mutex> guard(init_mutex);
        initialized = false;
    }
    init_cv.notify_all();

    std::unique_lock<std::mutex> guard(init_mutex);
    init_cv.wait(guard, [this]{ return initialized; });
}


void Chicken::init(const std::chrono::milliseconds & layingInterval)
{
    _name = randomName();
    _eggsLayed = 0;
    _layingInterval = layingInterval;
    noMoreEggs = false;
    _dead = false;

    std::unique_lock<std::mutex> guard(init_mutex);
    init_cv.wait(guard, [this]{ return !initialized; });
    initialized = true;
    init_cv.notify_all();
}

const std::string & Chicken::name() const
{
    return _name;
}

const std::string Chicken::randomName()
{
    std::random_device rnd;
    std::uniform_int_distribution<unsigned long> distr(0, possibleNames.size() - 1);

    return possibleNames[distr(rnd)];
}

int Chicken::eggsLayed()
{
    std::lock_guard<std::mutex> guard(eggsLayed_mutex);
    return _eggsLayed;
}

std::chrono::milliseconds Chicken::layingInterval() const
{
    return _layingInterval;
}

const std::chrono::milliseconds Chicken::randomLayingInterval()
{
    std::random_device rnd;
    std::uniform_int_distribution<unsigned> distr(minRandomLayingInterval_ms, maxRandomLayingInterval_ms);

    return 1ms * distr(rnd);
}

bool Chicken::awaitingButcher()
{
    std::lock_guard<std::mutex> guard(noMoreEggs_mutex);
    return noMoreEggs;
}

bool Chicken::dead()
{
    std::lock_guard<std::mutex> guard(dead_mutex);
    return _dead;
}

void Chicken::slay()
{
    noMoreEggs = true;
}

void Chicken::live(const std::chrono::milliseconds & layingInterval)
{
    init(layingInterval);

    bool noMoreEggs_copy;

    do
    {
        std::this_thread::sleep_for(_layingInterval);

        eggsLayed_mutex.lock();
        _eggsLayed++;
        eggsLayed_mutex.unlock();

        noMoreEggs_mutex.lock();
        noMoreEggs_copy = noMoreEggs;
        noMoreEggs_mutex.unlock();
    } while (!noMoreEggs_copy);

    std::lock_guard<std::mutex> guard(dead_mutex);
    _dead = true;
}

const std::vector<std::string> Chicken::possibleNames =
{
    "Anastasia",
    "Annie",
    "Arabella",
    "Beatrice",
    "Bessie",
    "Betsy",
    "Edna",
    "Charlotte",
    "Clarabelle",
    "Daisy",
    "Daphne",
    "Dorothy",
    "Dottie",
    "Estelle",
    "Eloise",
    "Felicity",
    "Florence",
    "Francis",
    "Genevieve",
    "Georgia",
    "Geraldine",
    "Gladys",
    "Gloria",
    "Harriet",
    "Holly",
    "Iris",
    "June",
    "Loretta",
    "Mabel",
    "Maude",
    "Minnie",
    "Matilda",
    "Myrtle",
    "Opal",
    "Pearl",
    "Penelope",
    "Polly",
    "Stella",
    "Sadie",
    "Tillie"
};
