#pragma once

#include <thread>
#include <string>
#include <vector>
#include <chrono>
#include <mutex>
#include <condition_variable>

class Chicken : public std::thread
{
public:
    Chicken(const std::chrono::milliseconds & layingInterval = randomLayingInterval());
    ~Chicken();

    void initFromConstructor();
    void init(const std::chrono::milliseconds & layingInterval);

    const std::string & name() const;
    static const std::string randomName();

    int eggsLayed();

    std::chrono::milliseconds layingInterval() const;
    static const std::chrono::milliseconds randomLayingInterval();

    bool awaitingButcher();
    bool dead();

    void slay();

private:
    void live(const std::chrono::milliseconds & layingInterval);

    bool initialized;
    std::mutex init_mutex;
    std::condition_variable init_cv;

    std::string _name;

    std::mutex eggsLayed_mutex;
    int _eggsLayed;

    std::chrono::milliseconds _layingInterval;

    std::mutex noMoreEggs_mutex;
    bool noMoreEggs;

    std::mutex dead_mutex;
    bool _dead;

    static const std::vector<std::string> possibleNames;
    static const unsigned maxRandomLayingInterval_ms = 10000;
    static const unsigned minRandomLayingInterval_ms = 2000;
};
