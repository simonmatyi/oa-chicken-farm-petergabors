#include <catch.hpp>

#include "chicken.h"

#include <iostream>

using namespace std::chrono_literals;

TEST_CASE("Check random name generator function")
{
    auto generatedName = Chicken::randomName();
    //std::cout << "The name is: " << generatedName << std::endl;
    REQUIRE(!generatedName.empty());
}

TEST_CASE("Check random laying interval generator function")
{
    auto generatedInterval = Chicken::randomLayingInterval();
    //std::cout << "The laying interval is: " << generatedInterval.count() << "ms" << std::endl;
    REQUIRE(generatedInterval <= 10s);
    REQUIRE(generatedInterval >= 2s);
}

TEST_CASE("Chicken name")
{
    SECTION("Initial random name")
    {
        Chicken chicken(10ms);

        REQUIRE(chicken.name().length() != 0);
        REQUIRE(chicken.name() != "");

        chicken.slay();
        chicken.join();
    }
}

TEST_CASE("Chicken egg laying")
{
    SECTION("No eggs layed initially")
    {
        Chicken chicken;

        REQUIRE(chicken.eggsLayed() == 0);
        REQUIRE(chicken.layingInterval() >= 2s);
        REQUIRE(chicken.layingInterval() <= 10s);

        chicken.slay();
        chicken.detach();
    }

    Chicken chicken(100ms);

    auto start = std::chrono::system_clock::now();

    SECTION("No eggs layed before the end of the first interval")
    {
        /* Measure the time it takes to lay the first egg */
        std::chrono::duration<double> timeTakenToLay = 0ms;
        while((chicken.eggsLayed()) < 1
              && (timeTakenToLay < 1s))
        {
            std::this_thread::sleep_for(1ms);
            timeTakenToLay = std::chrono::system_clock::now() - start;
        }
        REQUIRE(timeTakenToLay > 95ms);
        REQUIRE(timeTakenToLay < 105ms);
        REQUIRE(chicken.eggsLayed() == 1);
    }

    SECTION("Wait for the second egg")
    {
        std::this_thread::sleep_for(105ms);

        REQUIRE(chicken.eggsLayed() == 2);
    }

    SECTION("Wait for the last egg")
    {
        chicken.slay();
        chicken.join();

        REQUIRE(chicken.eggsLayed() == 3);
    }

    SECTION("The chicken is no more for sure")
    {
        std::this_thread::sleep_for(500ms);

        REQUIRE(chicken.eggsLayed() == 3);
    }
}
